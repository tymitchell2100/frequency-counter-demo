const button = document.getElementById("countButton")
const lettersDiv = document.getElementById("lettersDiv")
const wordsDiv = document.getElementById('wordsDiv')

button.addEventListener('click', () => {
    lettersDiv.innerHTML = ''
    wordsDiv.innerHTML = ''

    let typedText = document.getElementById("textInput").value

    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "")

    const letterCounts = {}

    // for (let i = 0; i < typedText.length; i++) {
    //     currentLetter = typedText[i]
        
    //     if(letterCounts[currentLetter] === undefined) {
    //         letterCounts[currentLetter] = 1;
    //     } else {
    //         letterCounts[currentLetter]++;
    //     }
    // }

    typedText.split('').forEach(currentLetter => {
        if(letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
        } else {
            letterCounts[currentLetter]++;
        }
    })

    for (let letter in letterCounts) {
        const span = document.createElement("span")
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ")
        span.appendChild(textContent)
        lettersDiv.appendChild(span)
    }

    const wordsCount = {}

    typedText.split(' ').forEach(currentWord => {
        if(!wordsCount[currentWord]) {
            wordsCount[currentWord] = 1
        } else {
            wordsCount[currentWord]++
        }
    })

    for (let word in wordsCount) {
        const span = document.createElement("span")
        const textContent = document.createTextNode('"' + word + "\": " + wordsCount[word] + ", ")
        span.appendChild(textContent)
        wordsDiv.appendChild(span)
    }
})